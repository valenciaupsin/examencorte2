package com.e.examencorte2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.e.examencorte2.database.Usuario;
import com.e.examencorte2.database.UsuarioDB;

public class RegistroActivity extends AppCompatActivity {

    private EditText user_et;
    private EditText password_et;
    private EditText repassword_et;
    private EditText name_et;
    private Button signUp_btn;
    private Button clear_btn;
    private UsuarioDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        user_et = findViewById(R.id.user_et);
        password_et = findViewById(R.id.password_et);
        repassword_et = findViewById(R.id.repassword_et);
        name_et = findViewById(R.id.name_et);
        signUp_btn = findViewById(R.id.signUp_btn);
        clear_btn = findViewById(R.id.clear_btn);

        signUp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user_et.getText().toString().equals("") || password_et.getText().toString().equals("") || repassword_et.getText().toString().equals("") || name_et.getText().toString().equals("")) {
                    Toast.makeText(RegistroActivity.this, "Campos vacios", Toast.LENGTH_SHORT).show();
                } else {
                    if(user_et.getText().toString().equals(password_et.getText().toString())) {
                        Usuario user = new Usuario();
                        String usuario = user_et.getText().toString();
                        String password = password_et.getText().toString();
                        String nombre = name_et.getText().toString();

                        user.setUsuario(usuario);
                        user.setContrasena(password);
                        user.setNombre(nombre);
                        db.openDatabase();
                        long idx = db.insertUsuario(user);
                        Toast.makeText(RegistroActivity.this, "usuario agregado", Toast.LENGTH_SHORT).show();
                        db.cerrar();
                        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                        startActivityForResult(intent, 0);
                    } else {
                        Toast.makeText(RegistroActivity.this, "Las contraseñas deben ser iguales", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        clear_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_et.setText("");
                name_et.setText("");
                password_et.setText("");
                repassword_et.setText("");
            }
        });

    }
}

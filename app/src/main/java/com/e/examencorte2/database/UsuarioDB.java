package com.e.examencorte2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class UsuarioDB {
    private Context context;
    private UsuarioDbHelper usuarioDbHelper;
    private SQLiteDatabase db;

    private String[] columnToRead = new String[]{
            DefinirTabla.Usuario.COLUMN_NAME_USUARIO,
            DefinirTabla.Usuario.COLUMN_NAME_NOMBRE,
            DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA,
    };

    public UsuarioDB(Context context) {
        this.context = context;
        this.usuarioDbHelper = new UsuarioDbHelper(this.context);
    }

    public void openDatabase() { db = usuarioDbHelper.getWritableDatabase();}

    public long insertUsuario(Usuario u){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Usuario.COLUMN_NAME_USUARIO, u.getUsuario());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_NOMBRE, u.getNombre());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA, u.getContrasena());


        return db.insert (DefinirTabla.Usuario. TABLE_NAME, null, values);
    }

    public long actualizarContacto(Usuario u, String usuario){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Usuario.COLUMN_NAME_USUARIO, u.getUsuario());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_NOMBRE, u.getNombre());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA, u.getContrasena());

        String criterio = DefinirTabla.Usuario.COLUMN_NAME_USUARIO + "=" + usuario;

        return db.update(DefinirTabla.Usuario. TABLE_NAME, values, criterio,null);
    }

    public long eliminarContacto(String usuario){
        String criterio = DefinirTabla.Usuario.COLUMN_NAME_USUARIO + "=" +usuario;
        return db.delete(DefinirTabla.Usuario.TABLE_NAME, criterio,null);
    }

    public Usuario leerUsuario(Cursor cursor) {
        Usuario c = new Usuario();
        c.setUsuario(cursor.getString( 0));
        c.setNombre(cursor.getString(1));
        c.setContrasena(cursor.getString(2));
        return c;
    }

    public Usuario getUsuario(String user, String password) {
        Usuario usuario = null;

        SQLiteDatabase db = this.usuarioDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Usuario.TABLE_NAME, columnToRead, DefinirTabla.Usuario.COLUMN_NAME_USUARIO + " = ? AND " + DefinirTabla.Usuario.COLUMN_NAME_CONTRASENA + " = ? ", new String[] {String.valueOf(user), String.valueOf(password)}, null,null,null);
        if(c.moveToFirst()) {
            usuario = leerUsuario(c);
        }
        c.close();
        return usuario;
    }

    public ArrayList<Usuario> allUsuarios() {
        ArrayList<Usuario> usuarios = new ArrayList();
        Cursor cursor = db.query(DefinirTabla.Usuario. TABLE_NAME,
                 null, null,  null,
                 null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Usuario u = leerUsuario(cursor);
            usuarios.add(u);
            cursor.moveToNext();
        }
        cursor.close();
        return usuarios;
    }

    public void cerrar() { usuarioDbHelper.close(); }
}

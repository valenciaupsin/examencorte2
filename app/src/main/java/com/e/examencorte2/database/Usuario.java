package com.e.examencorte2.database;

import java.io.Serializable;

public class Usuario implements Serializable {
    private String usuario;
    private String nombre;
    private String contrasena;

    public Usuario() {
        this.usuario = "";
        this.nombre = "";
        this.contrasena = "";
    }

    public Usuario(String usuario, String nombre, String contrasena) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.contrasena = contrasena;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}

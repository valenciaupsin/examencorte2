package com.e.examencorte2.database;

import android.provider.BaseColumns;

public final class DefinirTabla {
    public DefinirTabla() {}
    public static abstract class Usuario implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USUARIO = "usuario";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_CONTRASENA = "contrasena";
    }
}

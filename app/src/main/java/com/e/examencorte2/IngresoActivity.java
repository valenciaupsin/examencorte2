package com.e.examencorte2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class IngresoActivity extends AppCompatActivity {

    private TextView name_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);

        name_tv = findViewById(R.id.name_tv);

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        name_tv.setText(nombre);
    }
}

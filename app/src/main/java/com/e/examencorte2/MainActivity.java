package com.e.examencorte2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.e.examencorte2.database.Usuario;
import com.e.examencorte2.database.UsuarioDB;

public class MainActivity extends AppCompatActivity {

    private EditText user_et;
    private EditText password_et;
    private Button signIn_btn;
    private Button signUp_btn;
    private UsuarioDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user_et = findViewById(R.id.user_et);
        password_et = findViewById(R.id.password_et);
        signIn_btn = findViewById(R.id.signIn_btn);
        signUp_btn = findViewById(R.id.signUp_btn);

        signIn_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = user_et.getText().toString();
                String password = password_et.getText().toString();
                if(user_et.getText().toString().equals("") || password_et.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Campos vacios", Toast.LENGTH_SHORT).show();
                } else {
                    db.openDatabase();
                    Usuario usuario = db.getUsuario(user, password);
                    if(usuario == null) {
                        Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, IngresoActivity.class);
                        intent.putExtra("nombre", usuario.getNombre());
                        startActivityForResult(intent, 0);
                    }
                    db.cerrar();
                }


            }
        });

        signUp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_et.setText("");
                password_et.setText("");
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });

    }
}
